Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim H As Integer = CInt(Console.ReadLine())
			Dim targetBoard(H - 1) As String
			For i As Integer = 0 To UBound(targetBoard)
				targetBoard(i) = Console.ReadLine()
			Next i
			
            Dim costLantern  As Integer = CInt(Console.ReadLine())
            Dim costMirror   As Integer = CInt(Console.ReadLine())
            Dim costObstacle As Integer = CInt(Console.ReadLine())
            
            Dim maxMirrors   As Integer = CInt(Console.ReadLine())
            Dim maxObstacles As Integer = CInt(Console.ReadLine())
            
			Dim cl As New CrystalLighting()
			Dim ret() As String = cl.placeItems(targetBoard, _
                costLantern, costMirror, costObstacle, _
                maxMirrors, maxObstacles)

			Console.WriteLine(ret.Length)
			For Each r As String In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module