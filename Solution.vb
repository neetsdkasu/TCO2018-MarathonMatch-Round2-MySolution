Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp3 = System.Tuple(Of Integer, Integer, Integer)
Imports Tp6 = System.Tuple(Of Integer, Integer, Integer, Integer, Integer, Integer)

Public Class CrystalLighting
    Dim startTime As Integer
    Dim limitTime As Integer
    Dim rand As New Random(19831983)

    Const Wall     As Integer = -1
    Const Right    As Integer = 0
    Const Down     As Integer = 1
    Const Left     As Integer = 2
    Const Up       As Integer = 3
    Const Common   As Integer = 4
    Const Match    As Integer = 5
    Const Blue     As Integer = 1
    Const Yellow   As Integer = 2
    Const Red      As Integer = 4
    Const Obstacle As Integer = 100

    Dim H As Integer, W As Integer
    Dim board(,) As Integer
    Dim costLantern As Integer, costMirror As Integer, costObstacle As Integer
    Dim maxMirrors As Integer, maxObstacles As Integer

    Dim IDBoard(,) As Integer
    Dim reqColors(,) As  Integer
    Dim reqIDs(,,) As Integer
    Dim crystalCount As Integer
    Dim allCrystals As New List(Of Tp3)()
    Dim crystals As New Dictionary(Of Integer, Tp6)()

    Dim spaceTable(,,) As Integer
    Dim spaceID As Integer

    Public Function placeItems(targetBoard() As String, _
            costLantern As Integer, costMirror As Integer, costObstacle As Integer, _
            maxMirrors As Integer, maxObstacles As Integer) As String()

        startTime = Environment.TickCount
        limitTime = startTime + 9800

        ReadBoard(targetBoard)

        Me.costLantern  = costLantern
        Me.costMirror   = costMirror
        Me.costObstacle = costObstacle

        Me.maxMirrors   = maxMirrors
        Me.maxObstacles = maxObstacles

        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))

        InitReqTable()
        InitSpaceTable()
        
        Dim cryList() As Tp6 = crystals.Values.ToArray()
        
        Dim sol As Tuple(Of Integer, List(Of Tp3)) = Solve1(cryList)
        
        Dim time0 As Integer = Environment.TickCount
        Dim time1 As Integer = Environment.TickCount
        Dim diffT as Integer = time1 - time0
        Dim upd As Integer = 0
        For cycle As Integer = 0 To Integer.MaxValue
            time0 = time1
            time1 = Environment.TickCount
            diffT = Math.Max(diffT, time1 - time0)
            If time1 + diffT > limitTime Then
                Console.Error.WriteLine("cycle: {0}, upd: {1}", cycle, upd)
                Exit For
            End If
            For i As Integer = 0 To UBound(cryList) - 1
                Dim j As Integer = rand.Next(i, cryList.Length)
                Dim t As Tp6 = cryList(i)
                cryList(i) = cryList(j)
                cryList(j) = t
            Next i
            Dim tmpSol As Tuple(Of Integer, List(Of Tp3)) = Solve1(cryList)
            If tmpSol.Item1 > sol.Item1 Then
                sol = tmpSol
                upd += 1
            End If
        Next cycle
        
        Dim score As Integer = sol.Item1
        Console.Error.WriteLine("sc: {0}", score)
        Dim ans As List(Of Tp3) = sol.Item2
        Dim ret(ans.Count - 1) As String
        For i As Integer = 0 To UBound(ret)
            Dim a As Tp3 = ans(i)
            Dim item As String
            Select Case a.Item3
            Case Obstacle
                item = "X"
            Case Else
                item = a.Item3.ToString()
            End Select
            ret(i) = String.Format("{0} {1} {2}", a.Item1, a.Item2, item)
        Next i

        placeItems = ret
        
        Console.Error.WriteLine("time: {0} ms", Environment.TickCount - startTime)
    End Function

    Private Sub ReadBoard(targetBoard() As String)
        H = targetBoard.Length
        W = targetBoard(0).Length
        ReDim board(H - 1, W - 1), IDBoard(H - 1, W - 1)
        crystalCount = 0
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim ch As Char = targetBoard(i)(j)
                If ch = "."c Then
                    board(i, j) = 0
                ElseIf ch = "X"c Then
                    board(i, j) = Wall
                Else
                    board(i, j) = Asc(ch) - Asc("0")
                    crystalCount += 1
                    IDBoard(i, j) = crystalCount
                    allCrystals.Add(New Tp3(i, j, crystalCount))
                End If
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim id As Integer = IDBoard(i, j)
                If id = 0 Then Continue For
                Dim cnt As Integer = 0
                Dim spc As Integer = &HF
                If j = W - 1 OrElse board(i, j + 1) <> 0 Then
                    cnt += 1
                    spc = spc Xor (1 << Right)
                End If
                If j = 0 OrElse board(i, j - 1) <> 0 Then
                    cnt += 1
                    spc = spc Xor (1 << Left)
                End If
                If i = H - 1 OrElse board(i + 1, j) <> 0 Then
                    cnt += 1
                    spc = spc Xor(1 << Down)
                End If
                If i = 0 OrElse board(i - 1, j) <> 0 Then
                    cnt += 1
                    spc = spc Xor (1 << Up)
                End If
                If cnt = 4 Then Continue For
                If cnt = 3 Then
                    Select Case board(i, j)
                    Case Blue, Yellow, Red
                    Case Else
                        Continue For
                    End Select
                End If
                crystals(id) = New Tp6(i, j, id, board(i, j), cnt, spc)
            Next j
        Next i
    End Sub

    Private Sub InitReqTable()
        ReDim reqColors(H - 1, W - 1), reqIDs(3, H - 1, W - 1)
        For i As Integer = 0 To H - 1
            Dim curID As Integer = 0
            Dim color As Integer = 0
            For j As Integer = 0 To W - 1
                Select Case board(i, j)
                Case Wall
                    curID = 0
                    color = 0
                Case 0
                    reqColors(i, j) = reqColors(i, j) Or (color << (4 * Right))
                    reqIDs(Right, i, j) = curID
                Case Else
                    curID = IDBoard(i, j)
                    color = board(i, j)
                End Select
            Next j
            curID = 0
            color = 0
            For j As Integer = W - 1 To 0 Step -1
                Select Case board(i, j)
                Case Wall
                    curID = 0
                    color = 0
                Case 0
                    reqColors(i, j) = reqColors(i, j) Or (color << (4 * Left))
                    reqIDs(Left, i, j) = curID
                Case Else
                    curID = IDBoard(i, j)
                    color = board(i, j)
                End Select
            Next j
        Next i
        For j As Integer = 0 To W - 1
            Dim curID As Integer = 0
            Dim color As Integer = 0
            For i As Integer = 0 To H - 1
                Select Case board(i, j)
                Case Wall
                    curID = 0
                    color = 0
                Case 0
                    reqColors(i, j) = reqColors(i, j) Or (color << (4 * Down))
                    reqIDs(Down, i, j) = curID
                Case Else
                    curID = IDBoard(i, j)
                    color = board(i, j)
                End Select
            Next i
            curID = 0
            color = 0
            For i As Integer = H - 1 To 0 Step -1
                Select Case board(i, j)
                Case Wall
                    curID = 0
                    color = 0
                Case 0
                    reqColors(i, j) = reqColors(i, j) Or (color << (4 * Up))
                    reqIDs(Up, i, j) = curID
                Case Else
                    curID = IDBoard(i, j)
                    color = board(i, j)
                End Select
            Next i
        Next j
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim color As Integer = reqColors(i, j)
                If color = 0 Then Continue For
                Dim cAnd As Integer = &HF
                Dim cOr  As Integer = 0
                Dim cnt  As Integer = 0
                For k As Integer = 0 To 3
                    Dim c As Integer = (color >> (4 * k)) And &HF
                    If c = 0 Then Continue For
                    cAnd = cAnd And c
                    cOr  = cOr  Or  c
                    cnt += 1
                Next k

                color = color Or (cAnd << (4 * Common))
                If cAnd = cOr Then
                    color = color Or (cnt << (4 * Match))
                End IF
                reqColors(i, j) = color
                
            Next j
        Next i
    End Sub

    Private Sub InitSpaceTable()
        ReDim spaceTable(Math.Max(Right, Down), H - 1, W - 1)
        spaceID = 0
        For i As Integer = 0 To H - 1
            Dim isSpace As Boolean = False
            For j As Integer = 0 To W - 1
                If board(i, j) = 0 Then
                    If Not isSpace Then
                        spaceID += 1
                        isSpace = True
                    End If
                    spaceTable(Right, i, j) = spaceID
                ElseIf isSpace Then
                    isSpace = False
                End If
            Next j
        Next i
        For j As Integer = 0 To W - 1
            Dim isSpace As Boolean = False
            For i As Integer = 0 To H - 1
                If board(i, j) = 0 Then
                    If Not isSpace Then
                        spaceID += 1
                        isSpace = True
                    End If
                    spaceTable(Down, i, j) = spaceID
                ElseIf isSpace Then
                    isSpace = False
                End If
            Next i
        Next j
    End Sub

    Private Function Solve1(cryList() As Tp6) As Tuple(Of Integer, List(Of Tp3))
        Dim ret As New List(Of Tp3)()
        Dim tmp As New List(Of Tp3)()

        Dim placed(spaceID) As Integer
        Dim colCnt(crystalCount) As Integer
        Dim rayCol(crystalCount) As Integer
        Dim dts() As Tp6 = { _
            New Tp6(Right, Right,  0,  1,    -1, W - 1), _
            New Tp6(Left,  Right,  0, -1,    -1,     0), _
            New Tp6(Down,  Down,   1,  0, H - 1,    -1), _
            New Tp6(Up,    Down,  -1,  0,     0,    -1) _
        }
        For Each cry As Tp6 In cryList
            Dim cy As Integer = cry.Item1
            Dim cx As Integer = cry.Item2
            ' Dim id As Integer = cry.Item3
            Dim col As Integer = cry.Item4
            ' Dim cnt As Integer = cry.Item5
            Dim spc As Integer = cry.Item6
            For Each dt As Tp6 In dts
                If (spc And (1 << dt.Item1)) = 0 Then Continue For
                Dim dy As Integer = dt.Item3
                Dim dx As Integer = dt.Item4
                Dim sid As Integer = spaceTable(dt.Item2, cy + dy, cx + dx)
                If (col And placed(sid)) = 0 Then Continue For
                col = col Xor placed(sid)
                If col = 0 Then
                    Exit For
                End If
            Next dt
            If col = 0 Then Continue For
            Dim col1 As Integer = If((col And 1) <> 0, 1, col And 2)
            Dim col2 As Integer = col Xor col1
            For Each dt As Tp6 In dts
                If (spc And (1 << dt.Item1)) = 0 Then Continue For
                Dim dy As Integer = dt.Item3
                Dim dx As Integer = dt.Item4
                Dim sid As Integer = spaceTable(dt.Item2, cy + dy, cx + dx)
                If placed(sid) <> 0 Then Continue For
                Dim ey As Integer = dt.Item5
                Dim ex As Integer = dt.Item6
                Dim ai As Integer = If(dt.Item2 = Right, Down, Right)
                Dim y As Integer = cy
                Dim x As Integer = cx
                Dim sel As Tp3 = Nothing
                Do
                    y += dy
                    x += dx
                    If board(y, x) <> 0 Then Exit Do
                    Dim acs As Integer = spaceTable(ai, y, x)
                    If placed(acs) <> 0 Then Continue Do
                    Dim rc As Integer = reqColors(y, x)
                    Dim cm As Integer = (rc >> (4 * Common)) And &HF
                    Dim mt As Boolean = (rc >> (4 * Match)) <> 0
                    If cm = 0 Then Continue Do
                    If (cm And col1) <> 0 Then
                        sel = New Tp3(y, x, col1)
                        If mt AndAlso cm = col1 Then
                            Exit Do
                        End If
                    End IF
                    If (cm And col2) <> 0 Then
                        sel = New Tp3(y, x, col2)
                        If mt AndAlso cm = col2 Then
                            Exit Do
                        End If
                    End If
                Loop While y <> ey AndAlso x <> ex
                If sel Is Nothing Then Continue For
                tmp.Add(sel)
                y = sel.Item1
                x = sel.Item2
                Dim scol As Integer = sel.Item3
                For i As Integer = 0 To 3
                    Dim rid As Integer = reqIDs(i, y, x)
                    colCnt(rid) += 1 << (4 * scol)
                    rayCol(rid) = rayCol(rid) Or scol
                Next i
                Dim asc As Integer = spaceTable(ai, y, x)
                placed(sid) = scol
                placed(asc) = scol
                col = col Xor scol
                col1 = col1 And col
                col2 = col2 And col
                If col = 0 Then
                    Exit For
                End If
            Next dt
        Next cry
        
        Dim items(H - 1, W - 1) As Boolean
        Dim lants(spaceID) As Integer
        
        For Each t As Tp3 in tmp
            Dim y As Integer = t.Item1
            Dim x As Integer = t.Item2
            Dim sc As Integer = -costLantern
            For i As Integer = 0 To 3
                Dim rid As Integer = reqIDs(i, y, x)
                If rid = 0 Then Continue For
                Dim cry As Tp3 = allCrystals(rid - 1)
                Dim col As Integer = board(cry.Item1, cry.Item2)
                If rayCol(rid) = col Then
                    Dim cc As Integer = (colCnt(rid) >> (4 * t.Item3)) And &HF
                    Select Case col
                    Case Blue, Yellow, Red
                        If cc = 1 Then sc += 20
                    Case Else
                        If cc = 1 Then sc += 30
                    End Select
                Else
                    sc -= 10
                End If
            Next i
            Dim hid As Integer = spaceTable(Right, y, x)
            Dim vid As Integer = spaceTable(Down, y, x)
            If sc >= 0 Then
                items(y, x) = True
                lants(hid) = ret.Count
                lants(vid) = ret.Count
                ret.Add(t)
                Continue For
            End If
            placed(hid) = 0
            placed(vid) = 0
            For i As Integer = 0 To 3
                Dim rid As Integer = reqIDs(i, y, x)
                colCnt(rid) -= 1 << (4 * t.Item3)
                Dim cc As Integer = (colCnt(rid) >> (4 * t.Item3)) And &HF
                If cc = 0 Then
                    rayCol(rid) = rayCol(rid) Xor t.Item3
                End If
            Next i
        Next t
        
        Dim bads(crystalCount) As Integer
        Dim score As Integer = -ret.Count * costLantern
        For i As Integer = 1 To crystalCount
            Dim cry As Tp3 = allCrystals(i - 1)
            Dim col As Integer = board(cry.Item1, cry.Item2)
            If rayCol(i) = col Then
                Select Case col
                Case Blue, Yellow, Red
                    score += 20
                Case Else
                    score += 30
                End Select
            ElseIf (rayCol(i) And col) = col Then
                Dim tcol As Integer = rayCol(i) Xor col
                If (tcol And Blue) <> 0 Then
                    bads(i) += (colCnt(i) >> (4 * Blue)) And &HF
                End If
                If (tcol And Yellow) <> 0 Then
                    bads(i) += (colCnt(i) >> (4 * Yellow)) And &HF
                End If
                If (tcol And Red) <> 0 Then
                    bads(i) += (colCnt(i) >> (4 * Red)) And &HF
                End If
                score -= 10                          
            ElseIf rayCol(i) <> 0 Then
                If (rayCol(i) And Blue) <> 0 Then
                    bads(i) += (colCnt(i) >> (4 * Blue)) And &HF
                End If
                If (rayCol(i) And Yellow) <> 0 Then
                    bads(i) += (colCnt(i) >> (4 * Yellow)) And &HF
                End If
                If (rayCol(i) And Red) <> 0 Then
                    bads(i) += (colCnt(i) >> (4 * Red)) And &HF
                End If
                score -= 10
            End If
        Next i
        
        If maxObstacles > 0 Then
            Dim tag As New List(Of Tp6)()
            For i As Integer = 0 To H - 1
                For j As Integer = 0 To W - 1
                    If items(i, j) Then Continue For
                    If board(i, j) <> 0 Then Continue For
                    Dim hid As Integer = spaceTable(Right, i, j)
                    Dim vid As Integer = spaceTable(Down, i, j)
                    Dim color As Integer = reqColors(i, j)
                    Dim rid As Integer, cc As Integer, lan As Tp3
                    Dim dec As Integer = 0
                    If placed(hid) <> 0 Then
                        lan = ret(lants(hid))
                        If lan.Item2 > j Then
                            cc = (color >> (4 * Right)) And &HF
                            rid = reqIDs(Right, i, j)
                            If cc = placed(hid) Then
                                If (colCnt(rid) >> (4 * cc)) = 1 Then
                                    dec -= 20
                                End If
                            ElseIf (cc And placed(hid)) = 0 Then
                                If bads(rid) = 1 Then dec += 10                                
                            ElseIf rayCol(rid) <> cc Then                                
                                If bads(rid) = 1 Then dec += 10
                            Else
                                If (colCnt(rid) >> (4 * placed(hid))) = 1 Then
                                    dec -= 30
                                End If
                            End If
                        ElseIf lan.Item2 < j Then
                            cc = (color >> (4 * Left)) And &HF
                            rid = reqIDs(Left, i, j)
                            If cc = placed(hid) Then
                                If (colCnt(rid) >> (4 * cc)) = 1 Then
                                    dec -= 20
                                End If
                            ElseIf (cc And placed(hid)) = 0 Then
                                If bads(rid) = 1 Then dec += 10                                
                            ElseIf rayCol(rid) <> cc Then                                
                                If bads(rid) = 1 Then dec += 10
                            Else
                                If (colCnt(rid) >> (4 * placed(hid))) = 1 Then
                                    dec -= 30
                                End If
                            End If
                        End If
                    End If
                    If placed(vid) <> 0 Then
                        lan = ret(lants(vid))
                        If lan.Item1 > i Then
                            cc = (color >> (4 * Down)) And &HF
                            rid = reqIDs(Down, i, j)
                            If cc = placed(vid) Then
                                If (colCnt(rid) >> (4 * cc)) = 1 Then
                                    dec -= 20
                                End If
                            ElseIf (cc And placed(vid)) = 0 Then
                                If bads(rid) = 1 Then dec += 10                                
                            ElseIf rayCol(rid) <> cc Then                                
                                If bads(rid) = 1 Then dec += 10
                            Else
                                If (colCnt(rid) >> (4 * placed(vid))) = 1 Then
                                    dec -= 30
                                End If
                            End If
                        ElseIf lan.Item1 < i Then
                            cc = (color >> (4 * Up)) And &HF
                            rid = reqIDs(Up, i, j)
                            If cc = placed(vid) Then
                                If (colCnt(rid) >> (4 * cc)) = 1 Then
                                    dec -= 20
                                End If
                            ElseIf (cc And placed(vid)) = 0 Then
                                If bads(rid) = 1 Then dec += 10                                
                            ElseIf rayCol(rid) <> cc Then                                
                                If bads(rid) = 1 Then dec += 10
                            Else
                                If (colCnt(rid) >> (4 * placed(vid))) = 1 Then
                                    dec -= 30
                                End If
                            End If
                        End If
                    End If
                    If dec = 0 OrElse costObstacle >= dec Then Continue For
                    tag.Add(New Tp6(-dec, i, j, hid, vid, 0))
                Next j
            Next i
            tag.Sort()
            Dim obs(spaceID) As Boolean
            Dim rems = maxObstacles
            For Each t As Tp6 In tag
                Dim dec As Integer = -t.Item1
                Dim y   As Integer = t.Item2
                Dim x   As Integer = t.Item3
                Dim hid As Integer = t.Item4
                Dim vid As Integer = t.Item5
                If obs(hid) Then dec -= 10
                If obs(vid) Then dec -= 10
                If costObstacle >= dec Then Continue For
                ret.Add(New Tp3(t.Item2, t.Item3, Obstacle))
                items(t.Item2, t.Item3) = True
                obs(hid) = True
                obs(vid) = True
                rems -= 1
                score += dec - costObstacle
                If rems = 0 Then Exit For
            Next t
        End If
        
        Solve1 = New Tuple(Of Integer, List(Of Tp3))(score, ret)
    End Function

End Class